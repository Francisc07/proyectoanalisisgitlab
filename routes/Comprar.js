//Express es un modulo de Node Js que permite la renderizacion el llamado de funciones por medio de rutas
var express = require('express');
//Router es una funcion de Express que permite el erutamiento
var router = express.Router();
//la variable bd llama a la funcion de conexion con la base de datos desde BD.js
var bd = require('./BD');
//Esta hace un llamado del modulo encargado de enviar correos
var nodemailer = require('nodemailer');





//Esta funcion agrega datos a la tabla usuario
router.post("/AddOrden", function(req, res, next) {
    //Estas son las variables que se va a enviar a la base de datos.
    //El req.body.idUser hace un request del del body del HTML que llama a esta funcion y busca cualquier item que tenga como nombre "idUser"
    //y guarda el value de ese item en la variable idUser, igual con las demás variables
    const fecha = new Date();
    const idUser = req.body.cedula;
    const det = req.body.det;
    const tipo = req.body.tipo;
    const fechaOrden = fecha.getFullYear() + "-" + (fecha.getMonth() + 1) + "-" + fecha.getDate();
    const fechaEnt = req.body.fenchaEnt;
    const monto = req.body.monto;
    const prov = req.body.prov;
    const cant = req.body.cant;
    const dist = req.body.dist;
    const dir = req.body.dir;

    //Aquí se guardan los datos necesarios para envia el mail
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'enviosrapidos1474@gmail.com',
            pass: 'EnviosRapidos123'
        }
    });
    //Aqui se guarda el contenido del mail
    var mailOptions = {
        from: 'enviosrapidos1474@gmail.com',
        to: 'franciscoh90987@gmail.com',
        subject: 'Orden pendiente de revision',
        text: 'Ha llegado una nueva orden por parte de un cliente, siga el siguiente link para realiza su correspondiente verificación http://localhost:3000'
    };
    //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que agrega la orden de compra con los valores de las variables
    bd.query("INSERT INTO ORDEN (IDUSUARIO,DETALLE_ORDEN,TIPOENTREGA,FECHAORDEN,FECHAENTREGA,MONTO,PROVINCIA,CANTON,DISTRITO,DIRECCION,ESTADO) VALUES('" + idUser + "','" + det + "','" + tipo + "','" + fechaOrden + "','" + fechaEnt + "'," + monto + ",'" + prov + "','" + cant + "','" + dist + "','" + dir + "', 'PENDIENTE');", function(err, items) {
        //Se realiza la función con los valores(err, items)
        //err guarda el error en caso de que alguno ocurra
        //items guarda los valores que indican que la consulta se realizó con éxito

        //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
        if (err) {
            console.log(err);
            console.log(fechaOrden);
        } else {
            //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que agrega el detalle de la orden de compra

            bd.query("INSERT INTO ORDEN_DETALLE (IDUSUARIO,APROBADOR_1,APROBADOR_2,FECHAAPROBACION_1,FECHAAPROBACION_2,RESULTADOANALISIS_1,RESULTADOANALISIS_2,CONDICIONFINAL) VALUES ('" + idUser + "','100842638','101075091','Fecha Pendiente','Fecha Pendiente','PENDIENTE','PENDIENTE','PENDIENTE');", function(err, items) {
                //Se realiza la función con los valores(err, items)
                //err guarda el error en caso de que alguno ocurra
                //items guarda los valores que indican que la consulta se realizó con éxito

                //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                if (err) {
                    console.log(err);
                    console.log(fechaOrden);
                } else {

                    //Si todo sale bien se envia el correo con la informacion guardada anteriormente
                    transporter.sendMail(mailOptions, function(error, info) {
                        if (error) {
                            console.log(error);
                        } else {
                            //Si todo sale bien se notifica que la orden fue guardad exitosamente y se renderiza de nuevo la pagina
                            res.render('Compras', { msg: "Se ha agregado la orden de compra" })
                            console.log('Email sent: ' + info.response);
                        }
                    });
                }
            })

        }
    })
});





module.exports = router;