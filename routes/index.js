//Express es un modulo de Node Js que permite la renderizacion el llamado de funciones por medio de rutas
var express = require('express');
//Router es una funcion de Express que permite el erutamiento
var router = express.Router();
//la variable bd llama a la funcion de conexion con la base de datos desde BD.js
var bd = require('./BD');

/* Esta ruta dirige a la ventana de login */
router.get('/', function(req, res, next) {
    res.render('Login');


});
/* Esta ruta dirige a la ventana de regstro de usuarios */
router.get('/registro', function(req, res, next) {
    res.render('Admin');
});
/* Esta ruta dirige a la ventana de tablas de usuario*/
router.get('/userTable', function(req, res, next) {
    res.render('UserTables');
});

/* Esta ruta dirige a la ventana de inicio del cliente*/
router.get('/CliInit', function(req, res, next) {
    res.render('InicioCliente');
});

/* Esta ruta dirige al formulario de compra del cliente*/
router.get('/ComprasCli', function(req, res, next) {
    res.render('Compras');
});

router.get('/ApJInit', function(req, res, next) {
    res.render('InicioAprobadorJefe');
});

router.get('/AnalisisApJefe', function(req, res, next) {
    res.render('AnalisisAproJefe');
});

router.post('/InicioFin', function(req, res, next) {
    const rol = req.body.rolFin;
    if (rol == 3) {
        res.render('InicioFin1', { rol: "3" });
    }
    if (rol == 4) {
        res.render('InicioFin1', { rol: "4" });
    }
    if (rol == 5) {
        res.render('InicioFin1', { rol: "5" });
    }
});





module.exports = router;