//Express es un modulo de Node Js que permite la renderizacion el llamado de funciones por medio de rutas
var express = require('express');
//Router es una funcion de Express que permite el erutamiento
var router = express.Router();
//la variable bd llama a la funcion de conexion con la base de datos desde BD.js
var bd = require('./BD');


//Esta funcion agrega datos a la tabla usuario
router.post("/getDetOrden", function(req, res, next) {
    //Estas son las variables que se va a enviar a la base de datos.
    //El req.body.idUser hace un request del del body del HTML que llama a esta funcion y busca cualquier item que tenga como nombre "idUser"
    //y guarda el value de ese item en la variable idUser, igual con las demás variables
    const idUSer = req.body.idUsuario;
    //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos del detalle de la orden segun su usuario
    bd.query("SELECT * FROM ORDEN_DETALLE WHERE IDUSUARIO ='" + idUSer + "';", function(err, items) {
        //Se realiza la función con los valores(err, items)
        //err guarda el error en caso de que alguno ocurra
        //items guarda los valores que indican que la consulta se realizó con éxito

        //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
        if (err) {
            console.log(err);

        } else {
            // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
            res.render('PedidosUsuario', { records: items });

        }
    })
});





module.exports = router;