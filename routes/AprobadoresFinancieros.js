//Express es un modulo de Node Js que permite la renderizacion el llamado de funciones por medio de rutas
var express = require('express');
//Router es una funcion de Express que permite el erutamiento
var router = express.Router();
//la variable bd llama a la funcion de conexion con la base de datos desde BD.js
var bd = require('./BD');
//Esta hace un llamado del modulo encargado de enviar correos
var nodemailer = require('nodemailer');

//Aquí se guardan los datos necesarios para envia el mail
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'enviosrapidos1474@gmail.com',
        pass: 'EnviosRapidos123'
    }
});
//Aqui se guarda el contenido del mail
var mailOptions = {
    from: 'enviosrapidos1474@gmail.com',
    to: 'franciscoh90987@gmail.com',
    subject: 'Orden pendiente de revision',
    text: 'El aprobador financiero ha aprobado su pedido, siga el siguiente link para realiza su correspondiente verificación http://localhost:3000'
};
//Aqui se guarda el contenido del mail
var mailOptions2 = {
    from: 'enviosrapidos1474@gmail.com',
    to: 'franciscoh90987@gmail.com',
    subject: 'Orden pendiente de revision',
    text: 'El aprobador financiero ha rechazado su pedido, siga el siguiente link para realiza su correspondiente verificación http://localhost:3000'
};

//Esta funcion obtiene los datos de la tablaORDEN
router.post("/getPedidosApFin", function(req, res, next) {
    const rol = req.body.rolFin;
    const valRep = req.body.valRep;
    if (valRep == 'Si') {
        if (rol == "3") {
            //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
            bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1 AND 100000 AND MONTH(FECHAENTREGA) IN (1,2,3);", function(err, items) {
                //Se realiza la función con los valores(err, items)
                //err guarda el error en caso de que alguno ocurra
                //items guarda los valores que indican que la consulta se realizó con éxito

                //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                if (err) {
                    console.log(err);

                } else {
                    // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                    res.render('ReporteFin', { records: items, rol: "3" });

                }
            })
        } else {
            if (rol == "4") {
                //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 100000 AND 1000000 AND MONTH(FECHAENTREGA) IN (1,2,3);", function(err, items) {
                    //Se realiza la función con los valores(err, items)
                    //err guarda el error en caso de que alguno ocurra
                    //items guarda los valores que indican que la consulta se realizó con éxito

                    //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                    if (err) {
                        console.log(err);

                    } else {
                        // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                        res.render('ReporteFin', { records: items, rol: "4" });

                    }
                })
            } else {
                if (rol == "5") {
                    //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                    bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1000000 AND 100000000 AND MONTH(FECHAENTREGA) IN (1,2,3);", function(err, items) {
                        //Se realiza la función con los valores(err, items)
                        //err guarda el error en caso de que alguno ocurra
                        //items guarda los valores que indican que la consulta se realizó con éxito

                        //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                        if (err) {
                            console.log(err);

                        } else {
                            // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                            res.render('ReporteFin', { records: items, rol: "5" });

                        }
                    })
                }
            }
        }
    } else {
        if (rol == "3") {
            //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
            bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1 AND 100000 ;", function(err, items) {
                //Se realiza la función con los valores(err, items)
                //err guarda el error en caso de que alguno ocurra
                //items guarda los valores que indican que la consulta se realizó con éxito

                //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                if (err) {
                    console.log(err);

                } else {
                    // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                    res.render('AnalisisFin', { records: items, rol: "3" });

                }
            })
        } else {
            if (rol == "4") {
                //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 100000 AND 1000000;", function(err, items) {
                    //Se realiza la función con los valores(err, items)
                    //err guarda el error en caso de que alguno ocurra
                    //items guarda los valores que indican que la consulta se realizó con éxito

                    //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                    if (err) {
                        console.log(err);

                    } else {
                        // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                        res.render('AnalisisFin', { records: items, rol: "4" });

                    }
                })
            } else {
                if (rol == "5") {
                    //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                    bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1000000 AND 100000000;", function(err, items) {
                        //Se realiza la función con los valores(err, items)
                        //err guarda el error en caso de que alguno ocurra
                        //items guarda los valores que indican que la consulta se realizó con éxito

                        //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                        if (err) {
                            console.log(err);

                        } else {
                            // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                            res.render('AnalisisFin', { records: items, rol: "5" });

                        }
                    })
                }
            }
        }
    }

});

//Esta funciones la que realiza el rechazo o aprobacion del pedido
router.post("/uptEstadoFinPedido", function(req, res, next) {
    //Estas son las variables que se va a enviar a la base de datos.
    //El req.body.idOrden hace un request del del body del HTML que llama a esta funcion y busca cualquier item que tenga como nombre "idOrden"
    //y guarda el value de ese item en la variable idOrden, igual con las demás variables
    const fecha = new Date();
    const idOrden = req.body.idAct;
    const fechaApro = fecha.getFullYear() + "-" + (fecha.getMonth() + 1) + "-" + fecha.getDate();
    const estado = req.body.estadoAct;
    const rol = req.body.rolFin;
    //Se verifica el estado que el aprobador jefe le dio al pedido y si es "RECHAZADO" realiza la funcion correspondiente para rechazar pedidos
    if (estado == "RECHAZADO") {
        //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que hace el update del estado del pedido en caso de ser rechazado
        bd.query("UPDATE ORDEN_DETALLE SET  FECHAAPROBACION_2 = '" + fechaApro + "',RESULTADOANALISIS_2 = 'RECHAZADO',CONDICIONFINAL = 'RECHAZADO' WHERE IDDETORDEN = '" + idOrden + "';", function(err, items) {
            //Se realiza la función con los valores(err, items)
            //err guarda el error en caso de que alguno ocurra
            //items guarda los valores que indican que la consulta se realizó con éxito

            //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
            if (err) {
                console.log(err);

            } else {
                //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query para eliminar la orden rechazada
                bd.query("UPDATE ORDEN SET ESTADO = 'RECHAZADO' WHERE IDORDEN = " + idOrden + ";", function(err, items) {
                    //Se realiza la función con los valores(err, items)
                    //err guarda el error en caso de que alguno ocurra
                    //items guarda los valores que indican que la consulta se realizó con éxito

                    //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                    if (err) {
                        console.log(err);

                    } else {
                        //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                        bd.query("SELECT * FROM ORDEN", function(err, items) {
                            //Se realiza la función con los valores(err, items)
                            //err guarda el error en caso de que alguno ocurra
                            //items guarda los valores que indican que la consulta se realizó con éxito

                            //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                            if (err) {
                                console.log(err);

                            } else {
                                transporter.sendMail(mailOptions2, function(error, info) {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                                        if (rol == "3") {
                                            //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                                            bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1 AND 100000 ;", function(err, items) {
                                                //Se realiza la función con los valores(err, items)
                                                //err guarda el error en caso de que alguno ocurra
                                                //items guarda los valores que indican que la consulta se realizó con éxito

                                                //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                                                if (err) {
                                                    console.log(err);

                                                } else {
                                                    // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                                                    res.render('AnalisisFin', { records: items, rol: "3" });

                                                }
                                            })
                                        }
                                        if (rol == "4") {
                                            //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                                            bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 100000 AND 1000000;", function(err, items) {
                                                //Se realiza la función con los valores(err, items)
                                                //err guarda el error en caso de que alguno ocurra
                                                //items guarda los valores que indican que la consulta se realizó con éxito

                                                //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                                                if (err) {
                                                    console.log(err);

                                                } else {
                                                    // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                                                    res.render('AnalisisFin', { records: items, rol: "4" });

                                                }
                                            })
                                        }
                                        if (rol == "5") {
                                            //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                                            bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1000000 AND 100000000;", function(err, items) {
                                                //Se realiza la función con los valores(err, items)
                                                //err guarda el error en caso de que alguno ocurra
                                                //items guarda los valores que indican que la consulta se realizó con éxito

                                                //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                                                if (err) {
                                                    console.log(err);

                                                } else {
                                                    // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                                                    res.render('AnalisisFin', { records: items, rol: "5" });

                                                }
                                            })
                                        }
                                        console.log('Email sent: ' + info.response);
                                    }
                                });

                            }
                        })

                    }
                })

            }
        })
    } else {
        //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que hace el update del estado del pedido en caso de ser aprobado
        bd.query("UPDATE ORDEN_DETALLE SET FECHAAPROBACION_2 = '" + fechaApro + "',RESULTADOANALISIS_2 = 'APROBADO',CONDICIONFINAL = 'APROBADO' WHERE IDDETORDEN = '" + idOrden + "';", function(err, items) {
            //Se realiza la función con los valores(err, items)
            //err guarda el error en caso de que alguno ocurra
            //items guarda los valores que indican que la consulta se realizó con éxito

            //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
            if (err) {
                console.log(err);

            } else {
                // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                bd.query("SELECT * FROM ORDEN", function(err, items) {
                    //Se realiza la función con los valores(err, items)
                    //err guarda el error en caso de que alguno ocurra
                    //items guarda los valores que indican que la consulta se realizó con éxito

                    //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                    if (err) {
                        console.log(err);

                    } else {
                        //Si todo sale bien se envia el correo con la informacion guardada anteriormente
                        transporter.sendMail(mailOptions, function(error, info) {
                            if (error) {
                                console.log(error);
                            } else {
                                console.log(rol)
                                if (rol == "3") {
                                    //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                                    bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1 AND 100000 ;", function(err, items) {
                                        //Se realiza la función con los valores(err, items)
                                        //err guarda el error en caso de que alguno ocurra
                                        //items guarda los valores que indican que la consulta se realizó con éxito

                                        //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                                        if (err) {
                                            console.log(err);

                                        } else {
                                            // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                                            res.render('AnalisisFin', { records: items, rol: "3" });

                                        }
                                    })
                                }
                                if (rol == "4") {
                                    //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                                    bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 100000 AND 1000000;", function(err, items) {
                                        //Se realiza la función con los valores(err, items)
                                        //err guarda el error en caso de que alguno ocurra
                                        //items guarda los valores que indican que la consulta se realizó con éxito

                                        //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                                        if (err) {
                                            console.log(err);

                                        } else {
                                            // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                                            res.render('AnalisisFin', { records: items, rol: "4" });

                                        }
                                    })
                                }
                                if (rol == "5") {
                                    //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                                    bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1000000 AND 100000000;", function(err, items) {
                                        //Se realiza la función con los valores(err, items)
                                        //err guarda el error en caso de que alguno ocurra
                                        //items guarda los valores que indican que la consulta se realizó con éxito

                                        //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                                        if (err) {
                                            console.log(err);

                                        } else {
                                            // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                                            res.render('AnalisisFin', { records: items, rol: "5" });

                                        }
                                    })
                                }
                                console.log('Email sent: ' + info.response);
                            }
                        });


                    }
                })

            }
        })
    }

});

//Esta funcion obtiene los datos de la tablaORDEN
router.post("/getPedidosReport", function(req, res, next) {
    const rol = req.body.rolFinVal;
    const valMes = req.body.valMes;
    if (valMes == 3) {
        if (rol == "3") {
            //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
            bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1 AND 100000 AND MONTH(FECHAENTREGA) IN (1,2,3);", function(err, items) {
                //Se realiza la función con los valores(err, items)
                //err guarda el error en caso de que alguno ocurra
                //items guarda los valores que indican que la consulta se realizó con éxito

                //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                if (err) {
                    console.log(err);

                } else {
                    // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                    res.render('ReporteFin', { records: items, rol: "3" });

                }
            })
        } else {
            if (rol == "4") {
                //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 100000 AND 1000000 AND MONTH(FECHAENTREGA) IN (1,2,3)", function(err, items) {
                    //Se realiza la función con los valores(err, items)
                    //err guarda el error en caso de que alguno ocurra
                    //items guarda los valores que indican que la consulta se realizó con éxito

                    //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                    if (err) {
                        console.log(err);

                    } else {
                        // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                        res.render('ReporteFin', { records: items, rol: "4" });

                    }
                })
            } else {
                if (rol == "5") {
                    //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                    bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1000000 AND 100000000 AND MONTH(FECHAENTREGA) IN (1,2,3)", function(err, items) {
                        //Se realiza la función con los valores(err, items)
                        //err guarda el error en caso de que alguno ocurra
                        //items guarda los valores que indican que la consulta se realizó con éxito

                        //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                        if (err) {
                            console.log(err);

                        } else {
                            // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                            res.render('ReporteFin', { records: items, rol: "5" });

                        }
                    })
                }
            }
        }
    } else {
        if (valMes == 6) {
            if (rol == "3") {
                //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1 AND 100000 AND MONTH(FECHAENTREGA) IN (1,2,3,4,5,6);;", function(err, items) {
                    //Se realiza la función con los valores(err, items)
                    //err guarda el error en caso de que alguno ocurra
                    //items guarda los valores que indican que la consulta se realizó con éxito

                    //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                    if (err) {
                        console.log(err);

                    } else {
                        // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                        res.render('ReporteFin', { records: items, rol: "3" });

                    }
                })
            } else {
                if (rol == "4") {
                    //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                    bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 100000 AND 1000000 AND MONTH(FECHAENTREGA) IN (1,2,3,4,5,6);", function(err, items) {
                        //Se realiza la función con los valores(err, items)
                        //err guarda el error en caso de que alguno ocurra
                        //items guarda los valores que indican que la consulta se realizó con éxito

                        //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                        if (err) {
                            console.log(err);

                        } else {
                            // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                            res.render('ReporteFin', { records: items, rol: "4" });

                        }
                    })
                } else {
                    if (rol == "5") {
                        //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                        bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1000000 AND 100000000 AND MONTH(FECHAENTREGA) IN (1,2,3,4,5,6);;", function(err, items) {
                            //Se realiza la función con los valores(err, items)
                            //err guarda el error en caso de que alguno ocurra
                            //items guarda los valores que indican que la consulta se realizó con éxito

                            //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                            if (err) {
                                console.log(err);

                            } else {
                                // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                                res.render('ReporteFin', { records: items, rol: "5" });

                            }
                        })
                    }
                }
            }
        } else {
            if (valMes == 12) {
                if (rol == "3") {
                    //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                    bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1 AND 100000 ;", function(err, items) {
                        //Se realiza la función con los valores(err, items)
                        //err guarda el error en caso de que alguno ocurra
                        //items guarda los valores que indican que la consulta se realizó con éxito

                        //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                        if (err) {
                            console.log(err);

                        } else {
                            // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                            res.render('ReporteFin', { records: items, rol: "3" });

                        }
                    })
                } else {
                    if (rol == "4") {
                        //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                        bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 100000 AND 1000000;", function(err, items) {
                            //Se realiza la función con los valores(err, items)
                            //err guarda el error en caso de que alguno ocurra
                            //items guarda los valores que indican que la consulta se realizó con éxito

                            //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                            if (err) {
                                console.log(err);

                            } else {
                                // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                                res.render('ReporteFin', { records: items, rol: "4" });

                            }
                        })
                    } else {
                        if (rol == "5") {
                            //esta funcion llama a bd que realiza la conexion con la base de datos y envía el query que obtiene los datos de las ordenes
                            bd.query("SELECT * FROM ORDEN WHERE ESTADO = 'APROBADO' AND MONTO BETWEEN 1000000 AND 100000000;", function(err, items) {
                                //Se realiza la función con los valores(err, items)
                                //err guarda el error en caso de que alguno ocurra
                                //items guarda los valores que indican que la consulta se realizó con éxito

                                //Si ocurre algún error a la hora de realizar la consulta, este se podrá observar en la consola
                                if (err) {
                                    console.log(err);

                                } else {
                                    // si todo sale bien se obtienen los datos de la tabla y se renderiza la pagina, cargando en la tabla correspondiente sus respectivos datos
                                    res.render('ReporteFin', { records: items, rol: "5" });

                                }
                            })
                        }
                    }
                }
            }
        }
    }


});

module.exports = router;